#pragma once
#include "Object.h"
class Visitor2 :
	public Object
{
public:
	Visitor2();
	~Visitor2();

	void Draw();
	void DrawLeg();
	void DrawBody();
	void DrawHead();
	void DrawArm();

	void cylinder(float h, float r);

	void Update(const double& deltaTime);

private:
	double r1 = 0.0f;
	double r2 = 0.0f;
	double r3 = 0.0f;
	int count1 = 0;
	int count2 = 0;

	int texId1;
	int texId2;
	int texId3;
	int texId4;
	int texId5;
	int texId6;
	int texId7;
	int texId8;
	int texId9;

};


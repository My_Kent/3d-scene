#pragma once
#include "Object.h"
class Helicopter :
	public Object
{
public:
	Helicopter();
	~Helicopter();

	void Draw();
	void Update(const double& deltaTime);
	void HandleKey(int key, int state, int x, int y);
	
	
	void DrawCabin();
	void DrawLanding();
	void DrawTail();
	void DrawPropeller1();
	void DrawPropeller2();


	void DrawCuboid(float l, float h, float w);


private:
	GLenum lightNum;
	int texId;
	float xpos, ypos, zpos;
	double r1 = 0; 
	double r2 = 0;
	
	// variables for spot lights
	float position[4];
	float ambient[4];
	float diffuse[4];
	float specular[4];
	float attenuation[3];
	float spotDir[4];
	float spotExponent;
	float spotCutOff;
	float rx, ry, rz;

	int count = 0;
};


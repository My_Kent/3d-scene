
#define _USE_MATH_DEFINES
#include "Umbrella.h"


Umbrella::Umbrella(float x, float y, float z, int p)
{
	position[0] = x;
	position[1] = y;
	position[2] = z;
	if (p == 1)
	{
		texId = Scene::GetTexture("./u1.bmp");
	}
	else{
		texId = Scene::GetTexture("./u2.bmp");
	}

}


Umbrella::~Umbrella()
{
}

void Umbrella::Draw()
{
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glTranslatef(position[0], position[1], position[2]);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texId);



	// Draw umbrella cover
	glBegin(GL_TRIANGLE_FAN);
	glTexCoord2f(0.5f, 0.5f);
	glVertex3f(0.0f, 25.0f, 0.0f);

	for (int i = 0; i <= 12; i++){
		glTexCoord2f(0.5f + 0.5*cos(30 * i*M_PI / 180), 0.5f + 0.5*sin(30 * i*M_PI / 180));
		glVertex3f(40 * cos(30 * i*M_PI / 180), 10.0f, -40 * sin(30 * i*M_PI / 180));
	}
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	//	glEnable(GL_LIGHTING);

	glTranslatef(0.0f, -50.0f, 0.0f);

	glColor3f(0.8f, 0.8f, 0.8f);
	// Draw umbrella pole
	cylinder(75.0f, 2.5f);
	glPopAttrib();
	glPopMatrix();

}


void Umbrella::cylinder(float h, float r){
	float res = 0.1f*M_PI;          // resolution (in radians: equivalent to 18 degrees)
	float x, z;                      // define x and z coordinates

	glBegin(GL_QUAD_STRIP);
	for (float t = 0.f; t <= 2 * M_PI; t += res){   // iterate angles of a circle
		// Equation of a circle
		x = r*cos(t);           // x = radius*cos(angle);
		z = r*sin(t);           // z = radius*sin(angle);
		glVertex3f(x, h, z);    // top
		glVertex3f(x, 0.f, z);  // bottom
	}
	// Close the circle by creating a GL_QUAD between the first and last points
	glVertex3f(r, h, 0.f);      // top
	glVertex3f(r, 0.f, 0.f);    // bottom
	glEnd();
}

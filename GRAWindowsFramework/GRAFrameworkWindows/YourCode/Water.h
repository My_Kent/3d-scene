#pragma once
#include "Object.h"
class Water :
	public Object
{
public:
	Water();
	~Water();

	void Draw();
	void Update(const double& deltaTime);
	

private:
	int texId;
	double count = 0;
};



/*
Use this as the starting point to your work. Include the header file for each object in your scene, then add it to the scene using AddObjectToScene().
*/

#include "MyScene.h"
#include "Helicopter.h"
#include "Light.h"
#include "Bridge.h"
#include "Stage.h"
#include "Water.h"
#include "umbrella.h"
#include "Beach.h"
#include "Visitor1.h"
#include "Visitor2.h"
#include "Lamp.h"
#include "MyCamera.h"

// Constructor creates your CourseworkScene and initialises the base class Scene
MyScene::MyScene( int argc, char **argv, const char *title, const int windowWidth, const int windowHeight ) : Scene(argc, argv, title, windowWidth, windowHeight)
{
}

// Initialise your scene by adding all the objects you want to be in the scene here
void MyScene::Init()
{
    // set background colour
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	//Camera *c = new Camera();
	//MyCamera *myCamera = new MyCamera(c);
	//AddObjectToScene(myCamera);


	
	Stage *stage = new Stage();
	AddObjectToScene(stage);
	

	Light* l = new Light();
	AddObjectToScene(l);
	
	Water *w = new Water();
	AddObjectToScene(w);


	

	Bridge *b = new Bridge();
	AddObjectToScene(b);


	
	Helicopter *h = new Helicopter();
	AddObjectToScene(h);
	
	Lamp *lamp = new Lamp();
	AddObjectToScene(lamp);
	
	Beach *bc = new Beach();
	AddObjectToScene(bc);

	Visitor1 *v1 = new Visitor1();
	AddObjectToScene(v1);

	Visitor2 *v2 = new Visitor2();
	AddObjectToScene(v2);

	Umbrella *u1 = new Umbrella(-600.0f, 50.0f, -400.0f, 1);
	AddObjectToScene(u1);

	Umbrella *u2 = new Umbrella(-600.0f, 50.0f, -200.0f, 2);
	AddObjectToScene(u2);
	
	
	

}



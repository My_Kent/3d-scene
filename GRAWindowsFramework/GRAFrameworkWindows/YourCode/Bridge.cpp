#include "Bridge.h"


Bridge::Bridge()
{
}


Bridge::~Bridge()
{
}

void Bridge::Draw()
{
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glTranslatef(0.0f, 0.0f, -600.0f);
	glScalef(0.5f, 0.5f, 0.5f);

	


	glDisable(GL_COLOR_MATERIAL);                       // Stop/disable glColor3 calls from affecting the colour of the surface
	// specify the color for the bridge pier
	GLfloat mat_ambient[] = { 0.8, 0.6, 0.6, 1.0 };     // Define the ambient material colour property K_a
	GLfloat mat_diffuse[] = { 0.26, 0.28, 0.32, 1.0 };     // Define the diffuse material colour property K_d
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };    // Define the specular material colour property K_s
	GLfloat mat_shininess[] = { 50.0 };                // Define the shininess/specular exponent factor n ( capped between 0.0 and 128.0 )
	glPushAttrib(GL_LIGHTING);                          // Push/remember the materail settings before overriding the defaults

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);    // Pass the matrial properties into openGL implementation of the Phong reflectance model
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);
	
	// Draw Bridge Pier
	DrawPier0(-350);
	DrawPier0(350);
	DrawPier2(-600, -240);
	DrawPier2(600, 240);

	// Draw Bridge Deck
	DrawDeck();
	
	glPopAttrib();

	
	// specify the color for the bridge
	GLfloat mat_ambient1[] = { 0.97, 0.6, 0.17, 1.0 };     // Define the ambient material colour property K_a
	GLfloat mat_diffuse1[] = { 0.62, 0.19, 0.08, 1.0 };     // Define the diffuse material colour property K_d
	GLfloat mat_specular1[] = { 1.0, 1.0, 1.0, 1.0 };    // Define the specular material colour property K_s
	GLfloat mat_shininess1[] = { 50.0 };                // Define the shininess/specular exponent factor n ( capped between 0.0 and 128.0 )
	glPushAttrib(GL_LIGHTING);                          // Push/remember the materail settings before overriding the defaults

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient1);    // Pass the matrial properties into openGL implementation of the Phong reflectance model
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse1);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular1);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess1);
	DrawPier1(-350);
	DrawPier1(350);
	DrawPier3(-650);
	DrawPier3(650);
	DrawHandrail();
	DrawChain1(45.0f);
	DrawChain1(-45.0f);
	glPopAttrib();

	
                            // Pop/forget the material properties that where just set and return to the default ones
	glEnable(GL_COLOR_MATERIAL);                        // Re-enable the affect of glColour3 calls


	glPopAttrib();
	glPopMatrix();

}

void Bridge::DrawPier0(float offset)
{
	glPushMatrix();
	glTranslatef(offset, 0.0f, 0.0f);
	DrawCuboid(100.0f, 16.0f, 160.0f);
	glPopMatrix();
}


void Bridge::DrawPier1(float offset)
{

	glPushMatrix();
	glTranslatef(offset, 0.0f, 0.0f);


	glPushMatrix();
	glTranslatef(0.0f, 8.0f + 400 / 2, 40.0f);
	DrawCuboid(20.0f, 400.0f, 20.0f);
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0f, 8.0f + 400 / 2, -40.0f);
	DrawCuboid(20.0f, 400.0f, 20.0f);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 60.0f, 0.0f);
	glRotatef(30.0f, 1.f, 0.f, 0.f);
	DrawCuboid(10.0f, 126.0f, 10.f);
	glRotatef(-60.0f, 1.f, 0.f, 0.f);
	DrawCuboid(10.0f, 126.0f, 10.f);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(0.0f, 260.f, 0.f);
	DrawCuboid(10.0f, 10.0f, 60.f);
	glTranslatef(0.0f, 30.f, 0.f);
	DrawCuboid(10.0f, 10.0f, 60.f);

	glTranslatef(0.f, -15.f, -20.f);
	for (int i = 0; i < 10; i++)
	{
		DrawCuboid(2.f, 30.f, 2.f);
		glTranslatef(0.0f, 0.0f, 4.0f);

	}
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 350.f, 0.f);
	DrawCuboid(10.0f, 10.0f, 60.f);
	glTranslatef(0.0f, 30.f, 0.f);
	DrawCuboid(10.0f, 10.0f, 60.f);

	glTranslatef(0.f, -15.f, -20.f);
	for (int i = 0; i < 10; i++)
	{
		DrawCuboid(2.f, 30.f, 2.f);
		glTranslatef(0.0f, 0.0f, 4.0f);

	}
	glPopMatrix();
	glPopMatrix();
}


void Bridge::DrawPier2(float offset1, float offset2)
{
	glPushMatrix();
	glTranslatef(offset1, 70.0f, 40.0f);
	DrawCuboid(40.0f, 160.0f, 10.0f);
	glTranslatef(offset2, 0.0f, 0.0f);
	DrawCuboid(40.0f, 160.0f, 10.0f);
	glTranslatef(-offset2, 0.0f, 0.0f);

	glTranslatef(0.0f, 0.0f, -80.0f);
	DrawCuboid(40.0f, 160.0f, 10.0f);
	glTranslatef(offset2, 0.0f, 0.0f);
	DrawCuboid(40.0f, 160.0f, 10.0f);
	glPopMatrix();

}

void Bridge::DrawPier3(float offset)
{
	
	
	glPushMatrix();

	glEnable(GL_AUTO_NORMAL);
	int nNumPoints = 3;
	if (offset < 0){

		float ctrlPoints[3][3][3] = { { { -200.0f, 60.0f, -35.0f }, { -20.0f, 80.0f, -35.0f }, { 40.0f, 0.0f, -35.0f } },
				
		{ { -200.0f, 60.0f, 0.0f }, { -20.0f, 80.0f, 0.0f }, { 40.0f, 0.0f, 0.0f } },
		{ { -200.0f, 60.0f, 35.0f }, { -20.0f, 80.0f, 35.0f }, { 40.0f, 0.0f, 35.0f } }};

		glTranslatef(offset, 0.0f, 0.0f);

		glMap2f(GL_MAP2_VERTEX_3, 0.0f, 10.0f, 3, 3, 0.0f, 10.0f, 9, 3, &ctrlPoints[0][0][0]);
		glEnable(GL_MAP2_VERTEX_3);
		glMapGrid2f(10, 0.0, 10.0, 10.0, 0.0, 10.0);
		glEvalMesh2(GL_FILL, 0, 10, 0, 10);
	}

	
	else{
		float ctrlPoints[3][3][3] = { { { -40.0f, 0.0f, -35.0f }, { 20.0f, 80.0f, -35.0f }, { 200.0f, 60.0f, -35.0f } },
		{ { -40.0f, 0.0f, 0.0f }, { 20.0f, 80.0f, 0.0f }, { 200.0f, 60.0f, 0.0f } },
		{ { -40.0f, 0.0f, 35.0f }, { 20.0f, 80.0f, 35.0f }, { 200.0f, 60.0f, 35.0f } }
		};
		glTranslatef(offset, 0.0f, 0.0f);

		glMap2f(GL_MAP2_VERTEX_3, 0.0f, 10.0f, 3, 3, 0.0f, 10.0f, 9, 3, &ctrlPoints[0][0][0]);
		glEnable(GL_MAP2_VERTEX_3);
		glMapGrid2f(10, 0.0, 10.0, 10.0, 0.0, 10.0);
		glEvalMesh2(GL_FILL, 0, 10, 0, 10);

	}

	glPopMatrix();

	
}


void Bridge::DrawDeck()
{
	glPushMatrix();
	glTranslatef(0.0f, 130.f, 0.0f);
	DrawCuboid(1700.0f, 10.0f, 80.0f);
	glPopMatrix();

}


void Bridge::DrawHandrail()
{
	glPushMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 100.f, 45.0f);
	DrawCuboid(1200.0f, 10.0f, 10.0f);
	glTranslatef(-600.f, 20.0f, 0.f);

	for (int i = 0; i < 30; i++){
		DrawCuboid(5.0f, 30.f, 5.f);
		glTranslatef(40.f, 0.0f, 0.f);
	}

	glPopMatrix();


	glPushMatrix();
	glTranslatef(0.0f, 100.f, -45.f);
	DrawCuboid(1200.0f, 10.0f, 10.0f);
	glTranslatef(-600.f, 20.0f, 0.f);

	for (int i = 0; i < 30; i++){
		DrawCuboid(5.0f, 30.f, 5.f);
		glTranslatef(40.f, 0.0f, 0.f);
	}
	glPopMatrix();
	glPopMatrix();
}


void Bridge::DrawCuboid(float l, float h, float w)
{
	glBegin(GL_QUADS);
	// Near Face
	glVertex3f(-l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, w / 2);
	// Right Face

	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	// Back Face
	glVertex3f(-l / 2, -h / 2, -w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	//glVertex3f(l / 2, -w / 2, -w / 2);
	// Left Face
	glVertex3f(-l / 2, -h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(-l / 2, -h / 2, -w / 2);
	// Bottom Face

	glVertex3f(-l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glVertex3f(-l / 2, -h / 2, -w / 2);
	

	// Top Face
	glVertex3f(-l / 2, h / 2, w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	
	glVertex3f(-l / 2, h / 2, -w / 2);
	glEnd();
}


void Bridge::DrawChain1(float offset)
{
	glPushMatrix();
	glTranslatef(0.0f, 100.f, offset);
	int numOfPoints = 3;
	float controlPoints[3][3] = { { 350.f, 300.f, 0.f }, { 0.f, -240.f, 0.f }, { -350.f, 300.f, 0.f } };
	glColor3f(0.5f, 0.5f, 0.5f);

	glMap1f(GL_MAP1_VERTEX_3, 0.0f, 100.0f, 3, numOfPoints, &controlPoints[0][0]);
	glEnable(GL_MAP1_VERTEX_3);
	glLineWidth(3.f);

	glBegin(GL_LINE_STRIP);
	for (int i = 0; i <= 100; i++)
	{
		glEvalCoord1f((float)i);
	}
	glEnd();
	glPopMatrix();
	DrawChain2(offset);
	DrawChain3(offset);

}

void Bridge::DrawChain2(float offset)
{
	glPushMatrix();
	glTranslatef(-600.0f, 140.f, offset);
	int numOfPoints = 3;
	float controlPoints[3][3] = { { 0.f, 0.f, 0.f }, { 250.f, 0.f, 0.f }, { 250.f, 250.f, 0.f } };
	glMap1f(GL_MAP1_VERTEX_3, 0.0f, 100.0f, 3, numOfPoints, &controlPoints[0][0]);
	glEnable(GL_MAP1_VERTEX_3);
	glLineWidth(3.f);

	glBegin(GL_LINE_STRIP);
	for (int i = 0; i <= 100; i++)
	{
		glEvalCoord1f((float)i);
	}
	glEnd();
	glPopMatrix();
}

void Bridge::DrawChain3(float offset)
{
	glPushMatrix();
	glTranslatef(350.0f, 140.f, offset);
	int numOfPoints = 3;
	float controlPoints[3][3] = { { 0.f, 250.f, 0.f }, { 0.f, 0.f, 0.f }, { 250.f, 0.f, 0.f } };
	glMap1f(GL_MAP1_VERTEX_3, 0.0f, 100.0f, 3, numOfPoints, &controlPoints[0][0]);
	glEnable(GL_MAP1_VERTEX_3);
	glLineWidth(3.f);

	glBegin(GL_LINE_STRIP);
	for (int i = 0; i <= 100; i++)
	{
		glEvalCoord1f((float)i);
	}
	glEnd();


	glPopMatrix();
}
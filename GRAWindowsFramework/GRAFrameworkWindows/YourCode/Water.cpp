#include "Water.h"
#include "MyScene.h"

Water::Water()
{
	texId = Scene::GetTexture("./water.bmp");
}


Water::~Water()
{
}

void Water::Draw()
{
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -300.0f);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texId);



	// using count to divide the initial picture into two pieces
	// draw the first piece
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(count*0.1,0.f);
	glVertex3f(-400.0f, 0.0f, 1000.f);
	glTexCoord2f(count*0.1, 1.f);
	glVertex3f(400.0f, 0.0f, 1000.f);
	glTexCoord2f(1.f, 1.f);
	glVertex3f(400.0f, 0.0f, 1000 - (2000 - count*200));
	glTexCoord2f(1.f, 0.f);
	glVertex3f(-400.0f, 0.0f, 1000 - (2000 - count * 200));
	glEnd();


	// draw second piece
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-400.0f, 0.0f, 1000 - (2000 - count * 200));
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(400.0f, 0.0f, 1000 - (2000 - count * 200));
	glTexCoord2f(0.1*count, 1.0f);
	glVertex3f(400.0f, 0.0f, -1000.0f);
	glTexCoord2f(0.1*count, 0.0f);
	glVertex3f(-400.0f, 0.0f, -1000.0f);
	glEnd();



	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
//	glEnable(GL_LIGHTING);
	glPopMatrix();
}


void Water::Update(const double& deltaTime)
{
	count += deltaTime;
	if (count >= 10){ count = 0; }
}

#pragma once
#include "Object.h"
class Bridge :
	public Object
{
public:
	Bridge();
	~Bridge();

	void Draw();
	void DrawPier0(float offset);
	void DrawPier1(float offset);
	void DrawPier2(float offset1, float offset2);
	void DrawPier3(float offset);

	void DrawDeck();
	void DrawHandrail();
	void DrawChain1(float offset);
	void DrawChain2(float offset);
	void DrawChain3(float offset);
	void DrawCuboid(float l, float h, float w);

};


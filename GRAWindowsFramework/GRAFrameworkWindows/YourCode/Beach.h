#pragma once
#include "Object.h"
class Beach :
	public Object
{
public:
	Beach();
	~Beach();
	void Draw();
	void DrawChair(float x, float y, float z);
	void DrawCuboid(float l, float h, float w);

private:
	int texId;
	int texId1;
	
};


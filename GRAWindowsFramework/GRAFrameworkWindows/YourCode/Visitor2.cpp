#define _USE_MATH_DEFINES
#include "Visitor2.h"


Visitor2::Visitor2()
{
	texId1 = Scene::GetTexture("./v2f1.bmp");
	texId2 = Scene::GetTexture("./v2f2.bmp");
	texId3 = Scene::GetTexture("./v2f3.bmp");
	texId4 = Scene::GetTexture("./v2f4.bmp");
	texId5 = Scene::GetTexture("./v2f5.bmp");
	texId6 = Scene::GetTexture("./v2b1.bmp");
	texId7 = Scene::GetTexture("./v2b23.bmp");
	texId8 = Scene::GetTexture("./v2b4.bmp");
	texId9 = Scene::GetTexture("./v2.bmp");



}


Visitor2::~Visitor2()
{
}


void Visitor2::Draw()
{
	glPushMatrix();
	glTranslatef(-300.0f, 5.0f, -200.0f);
	DrawBody();
	DrawLeg();
	DrawArm();
	DrawHead();
	glPopMatrix();
}

void Visitor2::DrawBody()
{
	glPushMatrix();
	GLfloat hwidth = 5.0f;
	GLfloat hheight = 15.0f;
	GLfloat hlength = 5.0f;

	glEnable(GL_TEXTURE_2D);


/*	// Font Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(hheight, -hwidth, hlength);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(hheight, -hwidth, -hlength);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(hheight, hwidth, -hlength);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(hheight, hwidth, hlength);
	glEnd();

	*/


	// Left Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-hheight, -hwidth, hlength);
	glTexCoord2f(1.0f, 1.0f);
	
	glVertex3f(hheight, -hwidth, hlength);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(hheight, hwidth, hlength);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-hheight, hwidth, hlength);
	glEnd();


	// Right Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(hheight, -hwidth, -hlength);
	glTexCoord2f(0.0f, 0.0f);
	
	glVertex3f(-hheight, -hwidth, -hlength);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-hheight, hwidth, -hlength);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(hheight, hwidth, -hlength);
	glEnd();


	// Back Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-hheight, -hwidth, -hlength);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-hheight, -hwidth, hlength);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-hheight, hwidth, hlength);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-hheight, hwidth, -hlength);
	glEnd();


	// Top Face
	glBindTexture(GL_TEXTURE_2D, texId8);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(hheight, hwidth, hlength);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(hheight, hwidth, -hlength);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-hheight, hwidth, -hlength);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-hheight, hwidth, hlength);
	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


void Visitor2::DrawLeg()
{
	glPushMatrix();
	glTranslatef(-15.0f, 0.0f, 0.0f);

	// draw right leg
	glTranslatef(0.0f, 0.0f, 3.0f);
	glRotatef(90.0f + r1, 0.0f, 0.0f, 1.0f);
	cylinder(30.0f, 2.0f);

	// draw left leg
	glRotatef(-r1, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, -6.0f);
	glRotatef(-r1, 0.0f, 0.0f, 1.0f);
	cylinder(30.0f, 2.0f);
	glPopMatrix();

}


void Visitor2::Update(const double& deltaTime)
{
	// update value of r1
	if (count1 == 0)
	{
		r1 += 100 * deltaTime;
		if (r1 > 30){ count1 = 1; }
	}
	if (count1 == 1)
	{
		r1 -= 100 * deltaTime;
		if (r1 < -30){ count1 = 0; }
	}


	r2 += 200 * deltaTime;
	if (r2 > 360){ r2 = 0; }

	// update value of r2
	if (count2 == 0)
	{
		r3 += 100 * deltaTime;
		if (r3 > 90){ count2 = 1; }
	}
	if (count2 == 1)
	{
		r3 -= 100 * deltaTime;
		if (r3 < 0){ count2 = 0; }
	}

}

void Visitor2::cylinder(float h, float r){
	double res = 0.1f*M_PI;          // resolution (in radians: equivalent to 18 degrees)
	float x, z;                      // define x and z coordinates
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texId9);

	glBegin(GL_QUAD_STRIP);
	for (float t = 0.f; t <= 2 * M_PI; t += res){   // iterate angles of a circle
		// Equation of a circle
		x = r*cos(t);           // x = radius*cos(angle);
		z = r*sin(t);           // z = radius*sin(angle);
		glTexCoord2f(0.05*t, 1.f);
		glVertex3f(x, h, z);    // top
		glTexCoord2f(0.05*t, 0.f);
		glVertex3f(x, 0.f, z);  // bottom
	}
	// Close the circle by creating a GL_QUAD between the first and last points
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(r, h, 0.f);      // top
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(r, 0.f, 0.f);    // bottom
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

}


void Visitor2::DrawArm()
{
	glPushMatrix();

	glTranslatef(8.0f, 0.0f, 0.0f);

	// Draw Left Arm
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -5.0f);
	glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(r2, 0.0f, 1.0f, 0.0f);
	glRotatef(-40.0f, 0.0f, 0.0f, 1.0f);
	cylinder(15.0f, 1.5f);
	glTranslatef(0.0f, 14.5f, 0.0f);
	glRotatef(-45.0f, 0.0f, 0.0f, 1.0f);
	
	cylinder(18.0f, 1.5f);
	glPopMatrix();

	// Draw Right Arm
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 5.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glRotatef(-r2 + 180, 0.0f, 1.0f, 0.0f);
	glRotatef(-40.0f, 0.0f, 0.0f, 1.0f);
	cylinder(15.0f, 1.5f);
	glTranslatef(0.0f, 14.5f, 0.0f);
	glRotatef(-45.0f, 0.0f, 0.0f, 1.0f);
	cylinder(18.0f, 1.5f);
	glPopMatrix();



	glPopMatrix();

}

void Visitor2::DrawHead()
{
	glPushMatrix();
	glTranslatef(15.0f, 0.0f, 0.0f);
	glRotatef(-r3, 1.0f, 0.0f, 0.0f);
	GLfloat width = 7.0f;


	glEnable(GL_TEXTURE_2D);

	
	// top face
	glBindTexture(GL_TEXTURE_2D, texId5);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(10.0f, -width, width);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(10.0f, -width, -width);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(10.0f, width, -width);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(10.0f, width, width);
	glEnd();

/*	// neck
	glBegin(GL_QUADS);
	glVertex3f(0.0f, -width, -width);
	glVertex3f(0.0f, -width, width);
	glVertex3f(0.0f, width, width);
	glVertex3f(0.0f, width, -width);
	glEnd();  */


	// left
	glBindTexture(GL_TEXTURE_2D, texId2);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(0.0f, width, width);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(0.0f, -width, width);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(10.0f, -width, width);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(10.0f, width, width);
	glEnd();


	// right
	glBindTexture(GL_TEXTURE_2D, texId3);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(0.0f, -width, -width);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(0.0f, width, -width);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(10.0f, width, -width);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(10.0f, -width, -width);
	glEnd();


	// back 
	glBindTexture(GL_TEXTURE_2D, texId4);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(0.0f, width, -width);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(0.0f, width, width);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(10.0f, width, width);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(10.0f, width, -width);
	glEnd();

	// font face
	glBindTexture(GL_TEXTURE_2D, texId1);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(0.0f, -width, width);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(0.0f, -width, -width);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(10.0f, -width, -width);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(10.0f, -width, width);
	glEnd();

	


	glPopMatrix();
}
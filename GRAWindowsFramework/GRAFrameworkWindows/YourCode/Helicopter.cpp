#define _USE_MATH_DEFINES
#include "Helicopter.h"


Helicopter::Helicopter() :xpos(-500.f), ypos(300.f), zpos(-500.f), lightNum(GL_LIGHT2)
{
	position[0] = xpos + 50;
	position[1] = ypos - 50;
	position[2] = zpos;
	position[3] = 1.0f;

	ambient[0] = 0.95f;
	ambient[1] = 0.15f;
	ambient[2] = 0.0f;
	ambient[3] = 1.0f;

	diffuse[0] = 0.9f;
	diffuse[1] = 0.0f;
	diffuse[2] = 0.0f;
	diffuse[3] = 1.0f;

	specular[0] = 1.0f;
	specular[1] = 1.0f;
	specular[2] = 1.0f;
	specular[3] = 1.0f;

	attenuation[0] = 1.0f;
	attenuation[1] = 0.0f;
	attenuation[2] = 0.0f;

	spotDir[0] = 0.5f;
	spotDir[1] = -1.5f;
	spotDir[2] = 0.0f;
	spotDir[3] = 1.0f;

	spotExponent = 5.0f;
	rx = ry = rz = 0.0f;
	spotCutOff = 80.0f;
	texId = Scene::GetTexture("./helicopter.bmp");

}


Helicopter::~Helicopter()
{
}

void Helicopter::Draw()
{
	
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glTranslatef(xpos, ypos, zpos);
	glDisable(GL_COLOR_MATERIAL);
	

	GLfloat mat_ambient[] = { 0.9, 0.9, 0.0, 1.0 };     // Define the ambient material colour property K_a
	GLfloat mat_diffuse[] = { 0.9, 0.0, 0.0, 1.0 };     // Define the diffuse material colour property K_d
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };    // Define the specular material colour property K_s
	GLfloat mat_shininess[] = { 100.0 };                // Define the shininess/specular exponent factor n ( capped between 0.0 and 128.0 )
	glPushAttrib(GL_LIGHTING);
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_ambient);    // Pass the matrial properties into openGL implementation of the Phong reflectance model
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, mat_shininess);

	DrawCabin();
	DrawPropeller1();
	DrawPropeller2();
	
	DrawLanding();
	DrawTail();
	



	glPopAttrib();                                      // Pop/forget the material properties that where just set and return to the default ones
	glPopAttrib();
	glEnable(GL_COLOR_MATERIAL);                        // Re-enable the affect of glColour3 calls
	glPopMatrix();


}

void Helicopter::Update(const double& deltaTime)
{
		xpos += 30*deltaTime;

		if (xpos > 900){ xpos = -800; }
		r1 += 600*deltaTime;
		if (r1 > 180){ r1 = r1 - 180; }
		if (r2 > 180){ r2 = r2 - 180; }
		r2 += 800 * deltaTime;



		if (count == 0)
		{
			rx += 100.0f*deltaTime;
			if (rx > 60){ count = 1; }
		}
		if (count == 1)
		{
			rx -= 100.0f*deltaTime;
			if (rx < -60){ count = 0; }
		}


		position[0] = xpos + 50;
		// turn lighting on
		glEnable(GL_LIGHTING);

		// enable the light we wish to use
		glEnable(lightNum);

		/* explanation of parameters in glLightfv(light, pname, params);
		light - Specifies a light (8 lights max), identified by symbolic names of the form GL_LIGHTi.
		pname - Specifies a light source parameter for light, which can be
		GL_AMBIENT, GL_DIFFUSE, GL_SPECULAR, GL_POSITION, GL_SPOT_CUTOFF, GL_SPOT_DIRECTION, GL_SPOT_EXPONENT,
		GL_CONSTANT_ATTENUATION,GL_LINEAR_ATTENUATION, and GL_QUADRATIC_ATTENUATION.
		params - Specifies a pointer to the value(s) that parameter pname of light source light will be set to.
		*/

		// Set the colours of each of the components of the light source
		glLightfv(lightNum, GL_AMBIENT, ambient);
		glLightfv(lightNum, GL_DIFFUSE, diffuse);
		glLightfv(lightNum, GL_SPECULAR, specular);

		// Set the lights attentuation i.e. how quickly the light fades as we move away
		glLightf(lightNum, GL_CONSTANT_ATTENUATION, attenuation[0]);
		glLightf(lightNum, GL_LINEAR_ATTENUATION, attenuation[1]);
		glLightf(lightNum, GL_QUADRATIC_ATTENUATION, attenuation[2]);

		// Set the lights spotlight parameters
		glLightf(lightNum, GL_SPOT_EXPONENT, spotExponent);
		glLighti(lightNum, GL_SPOT_CUTOFF, spotCutOff);

		// set the position and spotlight directions for the light source
		// the position and spotlight direction we supply for each light are transformed / multiplied by the current transformation matrix
		// that openGL holds automatically
		glPushMatrix();
		glLoadIdentity();
		// Spotlights have position and direction in world space
		// we need to ensure the current camera transformation is set. This ensures that the coordinates
		// we pass as the light postion get transformed by the current transformation matrix to move them
		// from world space coordinates, into view space coordinate where lighting calculations take place
		// HENCE CALL SetUpCamera() function to ensure we have the correct transfrom matrix
		// Set the lights spotlight parameters
		Scene::GetCamera()->SetUpCamera();

		// set the postion of the light source
		glLightfv(lightNum, GL_POSITION, position);

		// apply any rotation to the spotlight direction we may wish to apply here
		glRotatef(rx, 1.0, 0.0, 0.0);
		glRotatef(ry, 0.0, 1.0, 0.0);
		glRotatef(rz, 0.0, 0.0, 1.0);

		// set the spotlight direction
		glLightfv(lightNum, GL_SPOT_DIRECTION, spotDir);

		glPopMatrix();
		
		

		

}

void Helicopter::HandleKey(int key, int state, int x, int y)
{
	
}



void Helicopter::DrawCabin()
{
	
	glPushMatrix();
	int num1 = 20;
	int num2 = 20;
	float radius = 30.0f;
	glScalef(1.5f, 1.0f, 1.0f);

	float step_z = M_PI / num1;
	float step_xy = 2 * M_PI / num2;
	float x[4], y[4], z[4];
	GLfloat x0 = 0.0f;
	GLfloat y0 = 0.0f;
	GLfloat z0 = 0.0f;


	float angle_z = 0.0;
	float angle_xy = 0.0;
	int i = 0, j = 0;

//	glEnable(GL_TEXTURE_2D);
//	glBindTexture(GL_TEXTURE_2D, texId);

	glBegin(GL_QUADS);
	for (i = 0; i<num1; i++)
	{
		angle_z = i * step_z;
		 
		for (j = 0; j<num2; j++)
		{
			angle_xy = j * step_xy;

			x[0] = radius * sin(angle_z) * cos(angle_xy);
			y[0] = radius * sin(angle_z) * sin(angle_xy);
			z[0] = radius * cos(angle_z);

			x[1] = radius * sin(angle_z + step_z) * cos(angle_xy);
			y[1] = radius * sin(angle_z + step_z) * sin(angle_xy);
			z[1] = radius * cos(angle_z + step_z);

			x[2] = radius*sin(angle_z + step_z)*cos(angle_xy + step_xy);
			y[2] = radius*sin(angle_z + step_z)*sin(angle_xy + step_xy);
			z[2] = radius*cos(angle_z + step_z);

			x[3] = radius * sin(angle_z) * cos(angle_xy + step_xy);
			y[3] = radius * sin(angle_z) * sin(angle_xy + step_xy);
			z[3] = radius * cos(angle_z);
					
			glVertex3f(x0 + x[0], y0 + y[0], z0 + z[0]);
			
			glVertex3f(x0 + x[1], y0 + y[1], z0 + z[1]);
			
			glVertex3f(x0 + x[2], y0 + y[2], z0 + z[2]);
			
			glVertex3f(x0 + x[3], y0 + y[3], z0 + z[3]);

			
			/*
			for (int k = 0; k < 4; k++)
			{
				glVertex3f(x0 + x[k], y0 + y[k], z0 + z[k]);

			}
			*/
		}
	}
	glEnd();

//	glBindTexture(GL_TEXTURE_2D, 0);
//	glDisable(GL_TEXTURE_2D);
	//glutSolidSphere(30.0, 30, 30);
	glPopMatrix();

}
void Helicopter::DrawLanding()
{
	glPushMatrix();
	glTranslatef(20.0f, -30.0f, 10.0f);
	DrawCuboid(5.0, 15.0f, 5.0f);
	glTranslatef(-40.0f, 0.0f, 0.0f);
	DrawCuboid(5.0, 15.0f, 5.0f);
	glTranslatef(20.0f, -10.0f, 0.0f);
	DrawCuboid(80.0f, 5.0f, 5.0f);
	glTranslatef(-20.0f, 15.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -20.f);
	DrawCuboid(5.0, 15.0f, 5.0f);
	glTranslatef(40.0f, 0.0f, 0.0f);
	DrawCuboid(5.0, 15.0f, 5.0f);
	glTranslatef(-20.0f, -10.0f, 0.0f);
	DrawCuboid(80.0f, 5.0f, 5.0f);
	glPopMatrix();
}
void Helicopter::DrawTail()
{
	glPushMatrix();
	float l = 130.0f;
	float h = 20.0f;
	float w = 10.0f;
	glTranslatef(-50.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	// Near Face

	
	glVertex3f(-l / 2, -h / 2 + 12.0f, w / 2);
	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, w / 2);

	// Right Face
	
	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, w / 2);

	// Back Face
	
	glVertex3f(-l / 2, -h / 2 +12.0f, -w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	// Left Face
	
	glVertex3f(-l / 2, -h / 2+12.0f, w / 2);
	glVertex3f(-l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(-l / 2, -h / 2 +12.0f, -w / 2);

	// Top Face
	
	glVertex3f(-l / 2, -h / 2+12.0f, w / 2);
	glVertex3f(-l / 2, -h / 2 + 12.0f, -w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glVertex3f(l / 2, -h / 2, w / 2);
	// Bottom Face

	glVertex3f(-l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	glEnd();


	glPopMatrix();

}
void Helicopter::DrawPropeller1()
{
	glPushMatrix();
	glTranslatef(0.0f, 40.f, 0.0f);
	DrawCuboid(8.0f, 10.0f, 8.0f);
	glRotatef(r1, 0.0f, 1.0f, 0.0f);
	DrawCuboid(150.0f, 5.0f, 5.0f);
	glRotatef(r1 + 90, 0.0f, 1.0f, 0.0f);
	DrawCuboid(150.0f, 5.0f, 5.0f);
	glPopMatrix();

}
void Helicopter::DrawPropeller2()
{
	glPushMatrix();
	glTranslatef(-110.0f, 0.0f, 5.0f);
	DrawCuboid(5.0f, 5.0f, 10.0f);
	glTranslatef(0.0f, 0.0f, 5.0f);
	glRotatef(r2, 0.0f, 0.0f, 1.0f);
	DrawCuboid(60.0f, 5.0f, 5.0f);
	glRotatef(r2 + 90, 0.0f, 0.0f, 1.0f);
	DrawCuboid(60.0f, 5.0f, 5.0f);

	glPopMatrix();

}


void Helicopter::DrawCuboid(float l, float h, float w)
{
	glBegin(GL_QUADS);


	// Near Face
	glVertex3f(-l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, w / 2);

	// Right Face
	glVertex3f(l / 2, -h / 2, w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, w / 2);

	// Back Face
	glVertex3f(-l / 2, -h / 2, -w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, -w / 2, -w / 2);


	// Left Face
	glVertex3f(-l / 2, -h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(-l / 2, -h / 2, -w / 2);

	// Top Face
	glVertex3f(-l / 2, -h / 2, w / 2);
	glVertex3f(-l / 2, -h / 2, -w / 2);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glVertex3f(l / 2, -h / 2, w / 2);

	// Bottom Face

	glVertex3f(-l / 2, h / 2, w / 2);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, -w / 2);
	glVertex3f(l / 2, h / 2, w / 2);
	glEnd();
}
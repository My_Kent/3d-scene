
#include "Light.h"

#include <cmath>

Light::Light() : t(0.0), radius(1000.0f)
{
	// Direction Light (the 4th component is 0): like the sun, rays are parallel and every thing is light as though the light hit it at a given direction.
	// Positional Light (the 4th component is 1): like a lamp in a room, objects are light based on the position and distance from the light source.

	

	// light 1 will be a white position light


	ambient1[0] = 0.0;  
	ambient1[1] = 0.0;
	ambient1[2] = 0.0;
	ambient1[3] = 1.0;
	diffuse1[0] = 1.0;   
	diffuse1[1] = 1.0;
	diffuse1[2] = 0.8;
	diffuse1[3] = 1.0;
	specular1[0] = 1.0;  
	specular1[1] = 1.0;
	specular1[2] = 1.0;
	specular1[3] = 1.0;

	position1[0] = 0.0f; // Set the position of light 1
	position1[1] = 200.f;
	position1[2] = -600.0f;
	position1[3] = 1;   // Mark light 1 as a positional light source

	
}

Light::~Light()
{
}

void Light::Draw()
{
	// Some basic code to draw the position and direction of the light,
	// Note: we cannot draw the directional light source, as it has no position, just direction.

	// Disable lighting on this geometry it will only be coloured by a glColor3 call
	glDisable(GL_LIGHTING);

	// draw the red light that is positioned in world space
	glDisable(GL_CULL_FACE);
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glColor3f(1.0, 0.0, 0.0);
	glTranslatef(position1[0], position1[1], position1[2]);
	glutSolidSphere(10.0, 10, 10);
	glPopAttrib();
	glPopMatrix();
	
	glEnable(GL_CULL_FACE);

	glEnable(GL_LIGHTING);
}

void Light::Update(const double& deltatime)
{
	// animate the position of the light sources
	t += deltatime *1/2;
	position1[0] = radius*cos(t);
	position1[1] = radius*sin(t);
	

	// turn lighting on
	glEnable(GL_LIGHTING);

	// enable each of the lights we wish to use

	glEnable(GL_LIGHT0);


	/* set parameters in glLightfv(light, pname, params);
	light - Specifies a light (8 lights max), identified by symbolic names of the form GL_LIGHTi.

	pname - Specifies a light source parameter for light, which can be
	GL_AMBIENT, GL_DIFFUSE, GL_SPECULAR, GL_POSITION, GL_SPOT_CUTOFF, GL_SPOT_DIRECTION, GL_SPOT_EXPONENT,
	GL_CONSTANT_ATTENUATION,GL_LINEAR_ATTENUATION, and GL_QUADRATIC_ATTENUATION.

	params - Specifies a pointer to the value(s) that parameter pname will be set to.
	*/


	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient1);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse1);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular1);
	

	// Set the lights attentuation i.e. how quickly the light fades as it moves away
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.0025);


	// set the positions of lights. The current transformation matrix is emptied
	// by calling glLoadIdentity() to ensure the lights are set to where we want to set them

	glPushMatrix();
	glLoadIdentity();

	

	// SetUpCamera() will leads to lights being set in camera space

	// Scene::GetCamera()->SetUpCamera();
	
	glLightfv(GL_LIGHT0, GL_POSITION, position1);
	glPopMatrix();
}
#include "MyCamera.h"
#include <cmath>

MyCamera::MyCamera(Camera* c) : cam(c)
{
	cam->GetEyePosition(x, y, z);
	cam->GetViewDirection(rx, ry, rz);
}



MyCamera::~MyCamera()
{
}

void MyCamera::Draw()
{
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glTranslatef(x, y, z);
	// glRotatef();  some value consitent the View with Camera

	DrawMyCamera();
	glPopAttrib();
	glPopMatrix();
}
void MyCamera::Update(const double& deltatime)
{
	cam->GetEyePosition(x, y, z);
	cam->GetViewDirection(rx, ry, rz);
	r += 600 * deltatime;
	if (r > 180){ r = r - 180; }
}

void MyCamera::DrawMyCamera()
{
	glPushMatrix();

	glTranslatef(0.0f, 0.0f, -10.0f);
	DrawCuboid(5.0f, 5.0f, 5.0f);
	glTranslatef(0.0f, 5.0f, 0.0f);
	glRotatef(r, 0.0f, 1.0f, 0.0f);
	DrawCuboid(50.0f, 2.0f, 2.0f);
	glRotatef(r + 90, 0.0f, 1.0f, 0.0f);
	DrawCuboid(50.0f, 2.0f, 2.0f);
	
	glPopMatrix();
}

void MyCamera::DrawCuboid(float x1, float y1, float z1)
{
	glBegin(GL_QUADS);
	// Near Face
	glVertex3f(-x1 / 2, -y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, -y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, y1 / 2, z1 / 2);
	glVertex3f(-x1 / 2, y1 / 2, z1 / 2);
	// Right Face

	glVertex3f(x1 / 2, -y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, -y1 / 2, -z1 / 2);
	glVertex3f(x1 / 2, y1 / 2, -z1 / 2);
	glVertex3f(x1 / 2, y1 / 2, z1 / 2);
	// Back Face
	glVertex3f(-x1 / 2, -y1 / 2, -z1 / 2);
	glVertex3f(-x1 / 2, y1 / 2, -z1 / 2);
	glVertex3f(x1 / 2, y1 / 2, -z1 / 2);
	glVertex3f(x1 / 2, -y1 / 2, -z1 / 2);

	// Left Face
	glVertex3f(-x1 / 2, -y1 / 2, z1 / 2);
	glVertex3f(-x1 / 2, y1 / 2, z1 / 2);
	glVertex3f(-x1 / 2, y1 / 2, -z1 / 2);
	glVertex3f(-x1 / 2, -y1 / 2, -z1 / 2);
	// Bottom Face

	glVertex3f(-x1 / 2, -y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, -y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, -y1 / 2, -z1 / 2);
	glVertex3f(-x1 / 2, -y1 / 2, -z1 / 2);


	// Top Face
	glVertex3f(-x1 / 2, y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, y1 / 2, z1 / 2);
	glVertex3f(x1 / 2, y1 / 2, -z1 / 2);
	glVertex3f(-x1 / 2, y1 / 2, -z1 / 2);
	glEnd();
}
#pragma once
#include "Object.h"
class Umbrella :
	public Object
{
public:
	Umbrella(float x, float y, float z, int p);
	~Umbrella();

	void Draw();
	void cylinder(float h, float r);

private:
	float position[3];
	int texId;

};


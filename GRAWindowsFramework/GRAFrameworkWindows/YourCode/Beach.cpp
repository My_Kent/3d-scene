#include "Beach.h"


Beach::Beach()
{
	texId = Scene::GetTexture("./beach.bmp");
	texId1 = Scene::GetTexture("./chair.bmp");
}


Beach::~Beach()
{
}


void Beach::Draw()
{
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	// draw left beach
	glTranslatef(-600.0f, 0.0f, -300.0f);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texId);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-400.0f, 0.0f, 1000.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(200.0f, 0.0f, 1000.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(200.0f, 0.0f, -1000.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-400.0f, 0.0f, -1000.0f);
	glEnd();

	glPushMatrix();
	// draw right beach
	glTranslatef(1400.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-400.0f, 0.0f, 1000.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(200.0f, 0.0f, 1000.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(200.0f, 0.0f, -1000.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-400.0f, 0.0f, -1000.0f);
	glEnd();
	glPopMatrix();


	// Draw two beach chairs
	DrawChair(100.0f, 10.0f, -100.f);
	DrawChair(100.0f, 10.0f, 100.f);
	
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	
	glPopAttrib();
	
	
	glPopMatrix();
}

void Beach::DrawChair(float x, float y, float z)
{
	glPushMatrix();

	glTranslatef(x, y, z);

	glPushMatrix();
	glRotatef(-20.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(30.0f, 2.0f, 2.0f);
	glRotatef(20.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, -30.0f);
	glRotatef(-20.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(30.0f, 2.0f, 2.0f);
	glRotatef(20.0f, 0.0f, 0.0f, 1.0f);


	glTranslatef(10.0f, -1.0f, 15.0f);
	float offset = 10.0f;
	glRotatef(-20.0f, 0.0f, 0.0f, 1.0f);

	for (int i = 0; i < 3; i++)
	{
		DrawCuboid(5.0f, 1.0f, 30.0f);
		glTranslatef(- offset, 0.0f, 0.0f);
	}

	glPopMatrix();





	glPushMatrix();
	glTranslatef(-25.0f, 0.0f, 0.0f);
	glRotatef(20.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(30.0f, 2.0f, 2.0f);
	glRotatef(-20.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, -30.0f);
	glRotatef(20.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(30.0f, 2.0f, 2.0f);

	glRotatef(-20.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, 15.0f);


	glRotatef(20.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(10.0f, 0.0f, 0.0f);
	for (int i = 0; i < 3; i++)
	{
		DrawCuboid(5.0f, 1.0f, 30.0f);
		glTranslatef(-offset, 0.0f, 0.0f);
	}

	glPopMatrix();

	glPushMatrix();
	glTranslatef(-50.0f, 12.0f, 0.0f);
	glRotatef(-50.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(40.0f, 2.0f, 2.0f);
	glRotatef(50.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, -30.0f);
	glRotatef(-50.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(40.0f, 2.0f, 2.0f);
	
	
	glRotatef(50.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, 15.0f);
	glRotatef(-50.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(15.0f, 0.0f, 0.0f);
	for (int i = 0; i < 4; i++)
	{
		DrawCuboid(5.0f, 1.0f, 30.0f);
		glTranslatef(-offset, 0.0f, 0.0f);
	}

	glPopMatrix();

	
	glPushMatrix();
	glTranslatef(-50.0f, 0.0f, 0.0f);
	glRotatef(60.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(20.0f, 2.0f, 2.0f);
	glRotatef(-60.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, 0.0f, -30.0f);
	glRotatef(60.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(20.0f, 2.0f, 2.0f);
	glPopMatrix();

	glPopMatrix();

}



void Beach::DrawCuboid(float l, float h, float w)
{
	
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texId1);
	
	glBegin(GL_QUADS);
	// Near Face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-l / 2, -h / 2, w / 2);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(l / 2, -h / 2, w / 2);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(l / 2, h / 2, w / 2);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-l / 2, h / 2, w / 2);


	// Right Face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(l / 2, -h / 2, w / 2);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(l / 2, h / 2, -w / 2);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(l / 2, h / 2, w / 2);


	// Back Face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-l / 2, -h / 2, -w / 2);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(l / 2, h / 2, -w / 2);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(l / 2, -h / 2, -w / 2);
	



	// Left Face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-l / 2, -h / 2, w / 2);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-l / 2, h / 2, w / 2);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-l / 2, -h / 2, -w / 2);
	
	
	// Bottom Face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-l / 2, -h / 2, w / 2);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(l / 2, -h / 2, w / 2);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(l / 2, -h / 2, -w / 2);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-l / 2, -h / 2, -w / 2);


	// Top Face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-l / 2, h / 2, w / 2);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(l / 2, h / 2, w / 2);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(l / 2, h / 2, -w / 2);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-l / 2, h / 2, -w / 2);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);


}
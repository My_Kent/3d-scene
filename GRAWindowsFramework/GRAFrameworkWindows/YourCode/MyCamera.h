#pragma once
#include "Object.h"
class Camera;

class MyCamera :
	public Object
{
public:
	MyCamera(Camera* c);
	~MyCamera();
	void DrawMyCamera();
	void Draw();
	void DrawCuboid(float x1, float y1, float z1);
	void Update(const double& deltatime);
private:
	Camera* cam;

	float x;
	float y;
	float z;

	float rx;
	float ry;
	float rz;
	float r = 0;

};


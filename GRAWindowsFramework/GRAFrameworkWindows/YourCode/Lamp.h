#pragma once
#include "Object.h"
class Lamp :
	public Object
{
public:
	Lamp();
	~Lamp();

	void Draw();
	void DrawCuboid(float x1, float y1, float z1);
	void Update(const double& deltatime);


private:
	float position1[4], ambient1[4], diffuse1[4], specular1[4];

	float attenuation[3];
	float spotDir[4];
	float spotExponent;
	float spotCutOff;
};


#include "Stage.h"


Stage::Stage()
{
	skybox[0] = Scene::GetTexture("./stagel.bmp");

	skybox[1] = Scene::GetTexture("./stageb.bmp");

	skybox[2] = Scene::GetTexture("skybox_up.bmp");
	

}


Stage::~Stage()
{
}

//void Stage::setTextures(GLuint* _texids){
//	texids = _texids;                       // Store texture references in pointer array
//	toTexture = true;                       // Assume all loaded correctly
//	for (int i = 0; i < 6; i++)             // Check if any textures failed to load (NULL)    
//		if (texids[i] == NULL) toTexture = false;   // If one texture failed, do not display any
//}


void Stage::Draw()
{
	
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -300.0f);
	glScalef(1000.0f, 1000.0f, 1000.0f);
	glEnable(GL_TEXTURE_2D);

	// Far Face
	glBindTexture(GL_TEXTURE_2D, skybox[1]);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1, 0, -1);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1, 0, -1);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1, 1, -1);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1, 1, -1);
	glEnd();

	// Right Face
	glBindTexture(GL_TEXTURE_2D, skybox[0]);
	glBegin(GL_QUADS);
	glNormal3f(-1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1, 0, -1);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1, 0, 1);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1, 1, 1);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1, 1, -1);
	glEnd();

	// Near Face
	glBindTexture(GL_TEXTURE_2D, skybox[1]);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, -1.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1, 0, 1);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1, 0, 1);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1, 1, 1);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1, 1, 1);
	glEnd();

	// Left Face
	glBindTexture(GL_TEXTURE_2D, skybox[0]);
	glBegin(GL_QUADS);
	glNormal3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1, 0, 1);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1, 0, -1);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1, 1, -1);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1, 1, 1);
	glEnd();


	// Top Face
	glBindTexture(GL_TEXTURE_2D, skybox[2]);
	glBegin(GL_QUADS);
	glNormal3f(0.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1, 1, -1);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1, 1, -1);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1, 1, 1);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1, 1, 1);
	glEnd();



	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


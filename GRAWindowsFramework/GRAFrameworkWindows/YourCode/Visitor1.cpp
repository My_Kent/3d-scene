#define _USE_MATH_DEFINES
#include "Visitor1.h"


Visitor1::Visitor1()
{
	texId1 = Scene::GetTexture("./v1f1.bmp");
	texId2 = Scene::GetTexture("./v1f2.bmp");
	texId3 = Scene::GetTexture("./v1f3.bmp");
	texId4 = Scene::GetTexture("./v1f4.bmp");
	texId5 = Scene::GetTexture("./v1f5.bmp");

	texId6 = Scene::GetTexture("./v1b1.bmp");
	texId7 = Scene::GetTexture("./v1b2.bmp");

	texId8 = Scene::GetTexture("./v1a1.bmp");
	texId9 = Scene::GetTexture("./v1a23.bmp");
	texId10 = Scene::GetTexture("./v1a4.bmp");
	texId11 = Scene::GetTexture("./v1.bmp");


}


Visitor1::~Visitor1()
{
}


void Visitor1::Draw()
{
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glTranslatef(-450.0f, 0.0f, -300.0f);
	DrawLeg();
	DrawBody();

	glPopAttrib();
	
	glPopMatrix();
}

void Visitor1::DrawLeg()
{
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -3.0f);
	cylinder(30.0f, 2.0f);
	glTranslatef(0.0f, 0.0f, 6.0f);
	cylinder(30.0f, 2.0f);
	glPopMatrix();
}


void Visitor1::DrawBody()
{
	glPushMatrix();
	glTranslatef(0.0f, 30.0f, 0.0f);

	glEnable(GL_TEXTURE_2D);

	
	// Font Face
	glBindTexture(GL_TEXTURE_2D, texId8);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(5.0f, 10.0f, -5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(5.0f, 10.0f, 5.0f);
	glEnd();



	// Left Face
	glBindTexture(GL_TEXTURE_2D, texId9);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(5.0f, 10.0f, 5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-5.0f, 10.0f, 5.0f);
	glEnd();

	// Right Face
	glBindTexture(GL_TEXTURE_2D, texId9);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-5.0f, 10.0f, -5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(5.0f, 10.0f, -5.0f);
	glEnd();



	// Back Face
	glBindTexture(GL_TEXTURE_2D, texId10);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-5.0f, 10.0f, 5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-5.0f, 10.0f, -5.0);
	glEnd();


/*	// Top Face
	glVertex3f(5.0f, 10.0f, 5.0f);
	glVertex3f(5.0f, 10.0f, -5.0f);
	glVertex3f(-5.0f, 10.0f, -5.0f);
	glVertex3f(-5.0f, 10.0f, 5.0f);
	glEnd();  */


	


	glTranslatef(0.0f, 10.0f, 0.0f);
	glRotatef(r1, 1.0f, 0.0f, 0.0f);


	
	// Font Face

	glBindTexture(GL_TEXTURE_2D, texId6);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(5.0f, 30.0f, -5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(5.0f, 30.0f, 5.0f);
	glEnd();



	// Left Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(5.0f, 30.0f, 5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-5.0f, 30.0f, 5.0f);
	glEnd();


	// Right Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-5.0f, 30.0f, -5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(5.0f, 30.0f, -5.0f);
	glEnd();

	// Back Face
	glBindTexture(GL_TEXTURE_2D, texId7);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, -5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(-5.0f, 0.0f, 5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-5.0f, 30.0f, 5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-5.0f, 30.0f, -5.0);
	glEnd();


	// Top Face
	glBindTexture(GL_TEXTURE_2D, texId11);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(5.0f, 30.0f, 5.0f);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(5.0f, 30.0f, -5.0f);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-5.0f, 30.0f, -5.0f);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-5.0f, 30.0f, 5.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

	DrawArm();
	DrawHead();

	glPopMatrix();
}

void Visitor1::cylinder(float h, float r){
	float res = 0.1f*M_PI;          // resolution (in radians: equivalent to 18 degrees)
	float x, z;                      // define x and z coordinates

	glEnable(GL_TEXTURE_2D);


	// Font Face
	glBindTexture(GL_TEXTURE_2D, texId11);

	glBegin(GL_QUAD_STRIP);
	for (float t = 0.f; t <= 2 * M_PI; t += res){   // iterate angles of a circle
		// Equation of a circle
		x = r*cos(t);           // x = radius*cos(angle);
		z = r*sin(t);           // z = radius*sin(angle);
		
		glTexCoord2f(0.05*t, 1.f);
		glVertex3f(x, h, z);    // top
	
		glTexCoord2f(0.05*t, 0.f);
		glVertex3f(x, 0.f, z);  // bottom
	}
	// Close the circle by creating a GL_QUAD between the first and last points
	
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(r, h, 0.f);      // top
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(r, 0.f, 0.f);    // bottom
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
}




void Visitor1::DrawArm()
{
	glPushMatrix();

	// Draw Left Hand
	glTranslatef(0.0f, 20.0f, 5.0f);
	glRotatef(150.0f, 1.0f, 0.0f, 0.0f);
	cylinder(35.0f, 1.5f);


	// Draw Right Hand
	glRotatef(-120.0f, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, 0.0f, -10.0f);
	glRotatef(-80.0f, 1.0f, 0.0f, 0.0f);
	cylinder(15.0f, 1.5f);
	glTranslatef(0.0f, 15.0f, 0.0f);
	glRotatef(r1, 1.0f, 0.0f, 0.0f);
	cylinder(20.0f, 1.5f);

	glPopMatrix();
}




void Visitor1::DrawHead()
{
	glPushMatrix();
	glTranslatef(0.0f, 30.0f, 0.0f);

	float hwidth = 8.0f;
	float hlength = 8.0f;
	float height = 15.0f;

	
	glEnable(GL_TEXTURE_2D);
	
	
	// Font Face
	glBindTexture(GL_TEXTURE_2D, texId1);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(hwidth, 0.0f, hlength);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(hwidth, 0.0f, -hlength);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(hwidth, height, -hlength);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(hwidth, height, hlength);
	glEnd();

	
	// Left Face
	glBindTexture(GL_TEXTURE_2D, texId2);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-hwidth, 0.0f, hlength);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(hwidth, 0.0f, hlength);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(hwidth, height, hlength);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-hwidth, height, hlength);
	glEnd();
	


	// Right Face
	glBindTexture(GL_TEXTURE_2D, texId3);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(hwidth, 0.0f, -hlength);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(-hwidth, 0.0f, -hlength);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-hwidth, height, -hlength);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(hwidth, height, -hlength);
	glEnd();

	// Back Face
	glBindTexture(GL_TEXTURE_2D, texId4);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(-hwidth, 0.0f, -hlength);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(-hwidth, 0.0f, hlength);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-hwidth, height, hlength);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-hwidth, height, -hlength);
	glEnd();

	// Top Face
	glBindTexture(GL_TEXTURE_2D, texId5);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.f);
	glVertex3f(hwidth, height, hlength);
	glTexCoord2f(1.0f, 0.f);
	glVertex3f(hwidth, height, -hlength);
	glTexCoord2f(1.0f, 1.f);
	glVertex3f(-hwidth, height, -hlength);
	glTexCoord2f(0.0f, 1.f);
	glVertex3f(-hwidth, height, hlength);
	glEnd();


	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	
	glPopMatrix();
}



void Visitor1::Update(const double& deltaTime)
{
	
	
	
	if (count == 0)
	{
		r1 += 100 * deltaTime;
		if (r1 > 60){ count = 1; }
	} 
	if (count == 1)
	{
		r1 -= 100 * deltaTime;
		if (r1 < 0){ count = 0; }
	}

	

}
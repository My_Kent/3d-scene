#define _USE_MATH_DEFINES
#include "Lamp.h"


Lamp::Lamp()
{
	ambient1[0] = 0.0;
	ambient1[1] = 0.0;
	ambient1[2] = 0.0;
	ambient1[3] = 1.0;

	diffuse1[0] = 1.0;
	diffuse1[1] = 1.0;
	diffuse1[2] = 0.4;
	diffuse1[3] = 1.0;

	specular1[0] = 0.0;
	specular1[1] = 0.0;
	specular1[2] = 0.0;
	specular1[3] = 1.0;

	attenuation[0] = 1.0f;
	attenuation[1] = 0.0f;
	attenuation[2] = 0.0f;

	spotDir[0] = 0.0f;
	spotDir[1] = 0.0f;
	spotDir[2] = -1.0f;
	spotDir[3] = 1.0f;

	spotExponent = 128.0f;

	spotCutOff = 30.0f;


	position1[0] = -680.0f; // Set the position of light 1
	position1[1] = 240.f;
	position1[2] = -320.0f;
	position1[3] = 1;   // Mark light 1 as a positional light source


}


Lamp::~Lamp()
{
}

void Lamp::Draw()
{
	glPushMatrix();
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glTranslatef(-750.0f, 0.0f, -300.0f);
	

	// Draw pole
	DrawCuboid(5.0f, 200.0f, 5.0f);
	glTranslatef(0.0f, 200.0f, 0.0f);
	glRotatef(-30.0f, 0.0f, 0.0f, 1.0f);
	DrawCuboid(4.0f, 100.0f, 4.0f);
	glTranslatef(0.0f, 100.0f, 0.0f);
	glRotatef(30.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.0f, -3.5f, 0.0f);
	DrawCuboid(50.0f, 3.5f, 3.5f);
	glTranslatef(50.0f, -20.0f, 0.0f);
	DrawCuboid(2.0f, 20.0, 2.0f);

	// Draw shade

	
	glBegin(GL_TRIANGLE_FAN);
	glVertex3f(0.0f, 0.0f, 0.0f);
	for (int i = 0; i <= 12; i++)
	{
		glVertex3f(40 * cos(30 * i*M_PI / 180), -20.0f, -40 * sin(30 * i*M_PI / 180));
	}

	glEnd();

	glPopMatrix();
	glPushMatrix();
	glTranslatef(position1[0], position1[1], position1[2]);

	glDisable(GL_LIGHTING);
	glColor3f(1.0f, 1.0f, 0.4f);
	glutSolidSphere(10.0, 30, 30);

	glEnable(GL_LIGHTING);
	glPopAttrib();
	glPopMatrix();
}

void Lamp::DrawCuboid(float x1, float y1, float z1)
{
	glBegin(GL_QUADS);

	// font face
	glVertex3f(-x1, 0.0f, z1);
	glVertex3f(x1, 0.0f, z1);
	glVertex3f(x1, y1, z1);
	glVertex3f(-x1, y1, z1);

	// right face
	glVertex3f(x1, 0.0f, z1);
	glVertex3f(x1, 0.0f, -z1);
	glVertex3f(x1, y1, -z1);
	glVertex3f(x1, y1, z1);

	// left face
	glVertex3f(-x1, 0.0f, -z1);
	glVertex3f(-x1, 0.0f, z1);
	glVertex3f(-x1, y1, z1);
	glVertex3f(-x1, y1, -z1);

	//	back face
	glVertex3f(x1, 0.0f, -z1);
	glVertex3f(-x1, 0.0f, -z1);
	glVertex3f(-x1, y1, -z1);
	glVertex3f(x1, y1, -z1);
	glEnd();

	
}


void Lamp::Update(const double& deltatime)
{
	// turn lighting on
	glEnable(GL_LIGHTING);

	// enable each of the lights we wish to use

	glEnable(GL_LIGHT3);


	/* set parameters in glLightfv(light, pname, params);
	light - Specifies a light (8 lights max), identified by symbolic names of the form GL_LIGHTi.

	pname - Specifies a light source parameter for light, which can be
	GL_AMBIENT, GL_DIFFUSE, GL_SPECULAR, GL_POSITION, GL_SPOT_CUTOFF, GL_SPOT_DIRECTION, GL_SPOT_EXPONENT,
	GL_CONSTANT_ATTENUATION,GL_LINEAR_ATTENUATION, and GL_QUADRATIC_ATTENUATION.

	params - Specifies a pointer to the value(s) that parameter pname will be set to.
	*/


	glLightfv(GL_LIGHT3, GL_AMBIENT, ambient1);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, diffuse1);
	glLightfv(GL_LIGHT3, GL_SPECULAR, specular1);


	// Set the lights attentuation i.e. how quickly the light fades as it moves away
	glLightf(GL_LIGHT3, GL_LINEAR_ATTENUATION, 0.0025);


	// set the positions of lights. The current transformation matrix is emptied
	// by calling glLoadIdentity() to ensure the lights are set to where we want to set them

	glPushMatrix();
	glLoadIdentity();



	// SetUpCamera() will leads to lights being set in camera space

	Scene::GetCamera()->SetUpCamera();

	glLightfv(GL_LIGHT3, GL_POSITION, position1);
	glLightfv(GL_LIGHT3, GL_SPOT_DIRECTION, spotDir);
	glPopMatrix();
}
#pragma once
#include "Object.h"
class Light :
	public Object
{
public:
	Light();
	~Light();

	void Draw();
	void Update(const double& deltatime);

	// member variable to remember how long program has been running
	double t;

	// member variable to store the radius at which the lights rotate around
	double radius;

	// member variables for the reflectance model parameters for each of the light sources
	
	float position1[4], ambient1[4], diffuse1[4], specular1[4];
	
};


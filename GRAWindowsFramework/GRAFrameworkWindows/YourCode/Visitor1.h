#pragma once
#include "Object.h"
class Visitor1 :
	public Object
{
public:
	Visitor1();
	~Visitor1();

	void Draw();
	void DrawLeg();
	void DrawBody();
	void DrawHead();
	void DrawArm();

	void cylinder(float h, float r);

	void Update(const double& deltaTime);

private:
	int texId1;
	int texId2;
	int texId3;
	int texId4;
	int texId5;
	int texId6;
	int texId7;
	int texId8;
	int texId9;
	int texId10;
	int texId11;


	double r1 = 0.0f;
	int count = 0;

};

